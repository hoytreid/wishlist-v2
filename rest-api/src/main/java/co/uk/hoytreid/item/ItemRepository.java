package co.uk.hoytreid.item;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    @Query("select i from Item i where i.account.username=:#{principal}")
    List<Item> findAll();
}
