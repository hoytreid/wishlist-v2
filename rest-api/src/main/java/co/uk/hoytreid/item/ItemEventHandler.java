package co.uk.hoytreid.item;

import co.uk.hoytreid.account.Account;
import co.uk.hoytreid.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Item.class)
public class ItemEventHandler {
    private final AccountRepository accountRepository;

    @Autowired
    public ItemEventHandler(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @HandleBeforeCreate
    public void addAccountBasedOnLoggedInUser(Item item) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountRepository.findByUsername(username);
        item.setAccount(account);
    }
}
