import React, { Component } from 'react';
import { Text, ScrollView, StyleSheet, View } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import PropTypes from 'prop-types';

import AddItem from '../components/AddItem';
import Header from '../components/Header';
import ListItem from '../components/ListItem';
import LogoutButton from '../components/LogoutButton';

class ListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    }
  }

  sortItems = () => {
    let items = []
    for (let i = 0; i < this.props.screenProps.items.length; i++) {
      if (!this.props.screenProps.items[i].bought) {
        items.push(this.props.screenProps.items[i]);
      }
    }
    this.setState({items});
  }

  componentDidUpdate(prevProps) {
    if(this.props.screenProps.items !== prevProps.screenProps.items) {
      this.sortItems();
    }
  }

  componentDidMount() {
    this.props.screenProps.getItems();
    this.sortItems();
  }

  render() {
    if (this.props.screenProps.isLoading) {
      return (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Text style={styles.infoText}>Now Loading...</Text>
        </View>
      );
    } else {
      return (
        <ScrollView style={styles.container}>
          <Header title={'Wishlist'} noOfItems={this.state.items.length} />
          <AddItem getItems={this.props.screenProps.getItems} serverUrl={this.props.screenProps.serverUrl} />
          { this.state.items.length == 0 ?
            <Text style={styles.infoText}>{'You don\'t have any items in your list'}</Text>
            :
            this.state.items.map((item) =>
            <ListItem
              key={item.id}
              id={item.id}
              description={item.description}
              price={item.price}
              bought={item.bought}
              getItems={this.props.screenProps.getItems}
              serverUrl={this.props.screenProps.serverUrl}
            />)
          }
          <LogoutButton />
        </ScrollView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 54
  },

  infoText: {
    fontSize: 16,
    color: '#b9b5c3',
    textAlign: 'center'
  }
});

ListScreen.propTypes = {
  screenProps: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    getItems: PropTypes.func.isRequired,
    serverUrl: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.object).isRequired
  }).isRequired
}

export default ListScreen;
