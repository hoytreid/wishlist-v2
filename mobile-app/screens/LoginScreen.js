import React, { Component } from 'react';
import { AsyncStorage, KeyboardAvoidingView, Text, TextInput, TouchableOpacity, StyleSheet, View } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import PropTypes from 'prop-types';

import Header from '../components/Header';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  _signInAsync = () => {
    if (this.state.username == '' || this.state.password == '') {
      showMessage({
        message: 'Please input your username and password',
        type: 'danger',
        backgroundColor: '#bd515a',
        color: '#fff',
        floating: 'true'
      });
      return;
    }

    fetch(this.props.screenProps.serverUrl + '/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        showMessage({
          message: 'Username or Password incorrect',
          type: 'danger',
          backgroundColor: '#bd515a',
          color: '#fff',
          floating: 'true'
        });
      }
    })
    .then((response) => {
      AsyncStorage.setItem('userToken', response.token);
      this.props.navigation.navigate('App');
    })
    .catch((error) => {
      showMessage({
        message: 'There was an error loggin in',
        type: 'danger',
        backgroundColor: '#bd515a',
        color: '#fff',
        floating: 'true'
      });
    });
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding">
        <Header title={'Login'} />
          <View style={styles.inputs}>
          <TextInput
            style={[styles.loginInput, {marginTop: 40}]}
            placeholder={'Username'}
            placeholderTextColor={'#76747d'}
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            textContentType={'username'}
          />
          <TextInput
            style={styles.loginInput}
            placeholder={'Password'}
            placeholderTextColor={'#76747d'}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            textContentType={'password'}
            secureTextEntry={true}
          />
          <TouchableOpacity
            style={styles.loginButton}
            onPress={this._signInAsync}>
            <Text style={styles.loginButtonText}>{'Login'.toUpperCase()}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.registerButton}
            onPress={() => this.props.navigation.navigate('Register')}>
            <Text style={styles.registerButtonText}>{'Register'.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 20,
    paddingRight: 20
  },

  inputs: {
    alignItems: 'center'
  },

  loginInput: {
    color: '#23213d',
    borderBottomWidth: 1,
    borderBottomColor: '#b9b5c3',
    fontSize: 16,
    paddingBottom: 5,
    marginBottom: 40,
    width: 240
  },

  loginButton: {
    backgroundColor: '#4884d4',
    height: 40,
    width: 240,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },

  loginButtonText: {
    color: '#fff',
    fontSize: 16,
  },

  registerButton: {
    height: 40,
    width: 240,
    borderRadius: 5,
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },

  registerButtonText: {
    color: '#4884d4',
    fontSize: 16,
  }
});

export default LoginScreen;
