import React, { Component } from 'react';
import { AsyncStorage, KeyboardAvoidingView, Text, TextInput, TouchableOpacity, StyleSheet, View } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import PropTypes from 'prop-types';

import Header from '../components/Header';

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  register = () => {
    if (this.state.username == '' || this.state.password == '') {
      showMessage({
        message: 'Please input a username and password',
        type: 'danger',
        backgroundColor: '#bd515a',
        color: '#fff',
        floating: 'true'
      });
      return;
    }

    fetch(this.props.screenProps.serverUrl + '/register', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    })
    .then((response) => {
      if(response.ok) {
        this.props.navigation.navigate('Login')
        showMessage({
          message: 'Account registered!',
          type: 'info',
          backgroundColor: '#94e092',
          color: '#fff',
          floating: 'true'
        });
      } else {
        showMessage({
          message: 'There was an error registering an account',
          type: 'danger',
          backgroundColor: '#bd515a',
          color: '#fff',
          floating: 'true'
        });
      }
    })
    .catch((error) => {
      showMessage({
        message: 'There was an error registering an account',
        type: 'danger',
        backgroundColor: '#bd515a',
        color: '#fff',
        floating: 'true'
      });
    });
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding">
        <Header title={'Register'} />
          <View style={styles.inputs}>
          <TextInput
            style={[styles.loginInput, {marginTop: 40}]}
            placeholder={'Username'}
            placeholderTextColor={'#76747d'}
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            textContentType={'username'}
          />
          <TextInput
            style={styles.loginInput}
            placeholder={'Password'}
            placeholderTextColor={'#76747d'}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            textContentType={'password'}
            secureTextEntry={true}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={this.register}>
            <Text style={styles.buttonText}>{'Register'.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 20,
    paddingRight: 20
  },

  inputs: {
    alignItems: 'center'
  },

  loginInput: {
    color: '#23213d',
    borderBottomWidth: 1,
    borderBottomColor: '#b9b5c3',
    fontSize: 16,
    paddingBottom: 5,
    marginBottom: 40,
    width: 240
  },

  button: {
    backgroundColor: '#4884d4',
    height: 40,
    width: 240,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttonText: {
    color: '#fff',
    fontSize: 16,
  }
});

export default RegisterScreen;
