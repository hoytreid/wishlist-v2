import React, { Component } from 'react';
import { AsyncStorage, StyleSheet, View } from 'react-native';
import FlashMessage, { showMessage } from 'react-native-flash-message';

import { Navigation } from './components/Navigation';

class App extends Component {
  constructor() {
    super();
    this.state = {
      items: [],
      isLoading: true,
    };
  }

  getItems = () => {
    return fetch(serverUrl + '/items', {
      method: 'GET',
      headers: {
        'Cache-Control': 'no-cache'
      }
    })
    .then(response =>
      response.json())
    .then(data => {
      this.setState({
        items: data.items,
        isLoading: false
      });
    })
    .catch((error) => {
      showMessage({
        message: 'Error: ' + error,
        type: 'danger',
        backgroundColor: '#bd515a',
        color: '#fff',
        floating: 'true'
      });
      AsyncStorage.clear();
    });
  }

  render() {
    return (
      <View style={styles.pageWrapper}>
        <Navigation
          screenProps={{
            items: this.state.items,
            isLoading: this.state.isLoading,
            getItems: this.getItems,
            serverUrl: serverUrl}}
        />
        <FlashMessage position='top' />
      </View>
    )
  }
}

const serverUrl = 'http://ccffb26c.ngrok.io' + '/api';

const styles = StyleSheet.create({
  pageWrapper: {
    flex: 1,
    margin: 0,
    padding: 0,
    justifyContent: 'center'
  }
});

export default App;
