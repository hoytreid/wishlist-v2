import React, { Component } from 'react';
import { AsyncStorage, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { withNavigation } from 'react-navigation';
import { showMessage } from 'react-native-flash-message';
import { PropTypes } from 'prop-types';

class LogoutButton extends Component {
  constructor(props) {
    super(props);
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    showMessage({
      message: 'Logged out successfully!',
      type: 'info',
      backgroundColor: '#94e092',
      color: '#fff',
      floating: 'true'
    });
    this.props.navigation.navigate('Auth');
  }

  render() {
    return(
      <TouchableOpacity
        style={styles.button}
        onPress={this._signOutAsync}>
        <Text style={styles.buttonText} >{'Logout'.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center'
  },

  buttonText: {
    fontSize: 14,
    color: '#bd515a',
    marginTop: 40,
    marginBottom: 35
  },
});

export default withNavigation(LogoutButton);
