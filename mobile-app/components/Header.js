import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { PropTypes } from 'prop-types';

class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.header}>{ this.props.title }</Text>
        { this.props.noOfItems ?
        <Text style={styles.subheader}>{ this.props.noOfItems == 1 ? `${this.props.noOfItems} Item` : `${this.props.noOfItems} Items` }</Text>
        :
        null }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 35,
    marginBottom: 40
  },

  header: {
    color: '#23213d',
    fontSize: 36,
    fontWeight: 'bold',
    fontFamily: 'Arial',
    marginBottom: 9
  },

  subheader: {
    color: '#76747d',
    fontSize: 16,
    fontFamily: 'Arial'
  }
});

Header.propTypes = {
  title: PropTypes.string.isRequired,
  noOfItems: PropTypes.number
}

export default Header;
