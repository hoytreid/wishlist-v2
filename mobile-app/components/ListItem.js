import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { showMessage } from 'react-native-flash-message';
import { PropTypes } from 'prop-types';

class ListItem extends Component {
  constructor(props) {
    super(props);
  }

  updateStatus = () => {
    fetch(this.props.serverUrl + '/items/' + this.props.id, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        description: this.props.description,
        price: this.props.price,
        bought: true,
      })
    })
    .then((response) => {
      if (response.ok) {
        showMessage({
          message: 'Item bought!',
          type: 'info',
          backgroundColor: '#94e092',
          color: '#fff',
          floating: 'true'
        });
        this.props.getItems();
      } else {
        showMessage({
          message: 'Sorry, something went wrong!',
          type: 'danger',
          backgroundColor: '#bd515a',
          color: '#fff',
          floating: 'true'
        });
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  deleteItem = () => {
    fetch(this.props.serverUrl + '/items/' + this.props.id, {
      method: 'DELETE'
    })
    .then((response) => {
      if (response.ok) {
        showMessage({
          message: 'Item deleted',
          type: 'info',
          backgroundColor: '#94e092',
          color: '#fff',
          floating: 'true'
        });
        this.props.getItems();
      } else {
        showMessage({
          message: 'Sorry, something went wrong!',
          type: 'danger',
          backgroundColor: '#bd515a',
          color: '#fff',
          floating: 'true'
        });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.itemInfo}>
          <Text style={styles.description}>{ this.props.description }</Text>
          <Text style={styles.price}>£ { parseFloat(this.props.price).toFixed(2) }</Text>
        </View>
        <TouchableOpacity
          style={[styles.button, this.props.bought ? {backgroundColor: '#94e092'} : {backgroundColor: '#4884d4'}]}
          onPress={this.updateStatus}
          disabled={this.props.bought}>
          {this.props.bought ?
            <Ionicons
              name='md-checkmark'
              size={24}
              color='#fff'
            />
            :
            <Ionicons
              name='md-basket'
              size={24}
              color='#fff'
            />
          }
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, {backgroundColor: '#bd515a', marginLeft: 10}]}
          onPress={this.deleteItem}>
        <Ionicons
          name='md-close'
          size={24}
          color='#fff'
        />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 35
  },

  itemInfo: {
    flex: 4,
    alignItems: 'flex-start'
  },

  description: {
    color: '#23213d',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 9
  },

  price: {
    color: '#76747d',
    fontSize: 16,
  },

  button: {
    backgroundColor: '#4884d4',
    height: 50,
    width: 50,
    borderRadius: 50/2,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

ListItem.propTypes = {
  serverUrl: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  getItems: PropTypes.func.isRequired,
  bought: PropTypes.bool.isRequired
}

export default ListItem;
