import React, { Component } from 'react';
import { Keyboard, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { showMessage } from 'react-native-flash-message';
import { PropTypes } from 'prop-types';

class AddItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: '',
      price: '',
      disableSubmit: false
    };
  }

  submitItem = () => {
    this.setState({disableSubmit: true});
    Keyboard.dismiss();

    if (this.state.description == '' || this.state.price == '') {
      showMessage({
        message: 'Please input a description and price',
        type: 'danger',
        backgroundColor: '#bd515a',
        color: '#fff',
        floating: 'true'
      });
      this.setState({disableSubmit: false});
      return;
    }

    fetch(this.props.serverUrl + '/items', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        description: this.state.description,
        price: this.state.price
      })
    })
    .then((response) => {
      if (response.ok) {
        this.setState({
          description: '',
          price: ''
        });
        this.props.getItems();
        showMessage({
          message: 'Item successfully added!',
          type: 'info',
          backgroundColor: '#94e092',
          color: '#fff',
          floating: 'true'
        });
        this.setState({disableSubmit: false});
      } else {
        showMessage({
          message: 'Sorry, something went wrong!',
          type: 'danger',
          backgroundColor: '#bd515a',
          color: '#fff',
          floating: 'true'
        });
        this.setState({disableSubmit: false});
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputs}>
          <TextInput
            style={styles.descriptionInput}
            placeholder={'Add an Item'}
            placeholderTextColor={'#76747d'}
            maxLength={26}
            onChangeText={(description) => this.setState({description})}
            value={this.state.description}
          />
          <View style={styles.price}>
            <Text style={styles.currencyMarker}>£</Text>
            <TextInput
              style={styles.priceInput}
              placeholder={'0.00'}
              placeholderTextColor={'#76747d'}
              maxLength={10}
              onChangeText={(price) => this.setState({price})}
              value={this.state.price}
              keyboardType={'numeric'}
            />
          </View>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={this.submitItem}
          disabled={this.state.disableSubmit}>
          <Ionicons
            name='md-add'
            size={36}
            color='#fff'
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 60
  },

  inputs: {
    flex: 4,
    alignItems: 'flex-start',
    marginRight: 30
  },

  descriptionInput: {
    color: '#23213d',
    borderBottomWidth: 1,
    borderBottomColor: '#b9b5c3',
    fontSize: 16,
    paddingBottom: 5,
    marginBottom: 14,
    alignSelf: 'stretch'
  },

  price: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#b9b5c3',
    paddingBottom: 5,
  },

  currencyMarker: {
    color: '#23213d',
    fontSize: 16,
    marginRight: 10
  },

  priceInput: {
    color: '#23213d',
    fontSize: 16,
    width: 80
  },

  button: {
    backgroundColor: '#94e092',
    height: 50,
    width: 50,
    borderRadius: 50/2,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

AddItem.propTypes = {
  serverUrl: PropTypes.string.isRequired,
  getItems: PropTypes.func.isRequired
}

export default AddItem;
