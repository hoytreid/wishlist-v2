import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { PropTypes } from 'prop-types';

class NavigationBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.props.navigation.navigate('List')}>
          <Text
            style={[styles.buttonText, this.props.navigation.state.index == 0 ? styles.buttonActive : styles.buttonInactive]}>
            { 'List'.toUpperCase() }
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Bought')}>
          <Text
            style={[styles.buttonText, this.props.navigation.state.index == 1 ? styles.buttonActive : styles.buttonInactive]}>
            { 'Bought'.toUpperCase() }
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 54,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderTopColor: '#b9b5c3'
  },

  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonText: {
    fontSize: 16,
  },

  buttonActive: {
    color: '#23213d'
  },

  buttonInactive: {
    color: '#76747d'
  }
});

NavigationBar.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    state: PropTypes.shape({
      index: PropTypes.number.isRequired
    }).isRequired
  }).isRequired
}

export default NavigationBar;
