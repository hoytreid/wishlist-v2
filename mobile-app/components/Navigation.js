import React from 'react';
import { createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';

import NavigationBar from './NavigationBar';

import BoughtScreen from '../screens/BoughtScreen';
import ListScreen from '../screens/ListScreen';
import LoadingScreen from '../screens/LoadingScreen';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';

const AppNavigation = createBottomTabNavigator({
    List: ListScreen,
    Bought: BoughtScreen
  },
  {
    tabBarComponent: (props) => <NavigationBar {...props} />
  }
);

const AuthNavigation = createStackNavigator(
  { Login:
    {
      screen: LoginScreen,
      navigationOptions: { header: null }
    },
    Register:
      {
        screen: RegisterScreen,
        navigationOptions: { header: null }
      }
  });

export const Navigation = createSwitchNavigator(
  {
    Loading: LoadingScreen,
    App: AppNavigation,
    Auth: AuthNavigation
  },
  {
    initalRouteName: 'Loading'
  }
);
