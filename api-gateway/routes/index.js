const express = require('express');
const router = express.Router();
const apiController = require('../controllers/apiController');

let jwt;

router.use((req, res, next) => {
	if(jwt) {
		res.set({
			'Authorization': jwt
		});
	}
	next();
})

router.get('/login', (req, res) => {
	res.render('login');
})

router.post('/login', (req, res) => {
	apiController.login(req, res)
		.then((response) => {
			jwt = response.Authorization;
			res.redirect('/');
		})
		.catch((error) => {
			res.render('login', {page: 'login', message: 'Incorrect username or password', type: 'error'});
			console.log(error.statusCode);
		});
})

router.get('/register', (req, res) => {
	res.render('register');
});

router.post('/register', (req, res) => {
	apiController.register(req, res)
		.then((response) => {
			res.render('register', {page: 'register', message: 'Account registered', type: 'info'});
		})
		.catch((error) => {
			res.render('register', {page: 'register', message: error.message || 'Could not register account', type: 'error'});
		});
});

router.get('/logout', (req, res) => {
	jwt = null;
	res.redirect('/');
})

router.get('/', (req, res) => {
	if(res.get('Authorization')) {
		apiController.getItems(res)
		.then((body) => {
			return body._embedded.items.filter(item => !item.bought);
		})
		.then((items) => {
			res.render('list', {page: 'list', items: items});
		})
		.catch((error) => {
			console.log(error.statusCode);
			res.redirect('/login');
		});
	} else {
		res.redirect('/login');
	}
});

router.post('/', (req, res) => {
	apiController.submitItem(req, res)
    .then(() => {
			res.redirect('/');
		})
		.catch((error) => {
			console.log(error.statusCode);
		});
});

router.post('/buy/:id', (req, res) => {
	apiController.getItemById(req, res)
		.then(item => {
			req.body = {
				description: item.description,
				price: item.price,
				bought: true
			};
			apiController.updateItem(req, res)
				.then(() => {
					res.redirect('/');
				})
				.catch((error) => {
					console.log(error.statusCode);
				});
		})
		.catch((error) => {
			console.log(error.statusCode);
		});
});

router.get('/bought', (req, res) => {
	apiController.getItems(res)
		.then((body) => {
			return body._embedded.items.filter(item => item.bought);
		})
		.then((items) => {
			res.render('bought', {page: 'bought', items: items});
		})
		.catch((error) => {
			console.log(error.statusCode);
			res.redirect('/login');
		});
});

router.post('/delete/:id', (req, res) => {
	apiController.deleteItem(req, res)
		.then(() => {
			res.redirect('back');
		})
		.catch((error) => {
			console.log(error.statusCode);
		});;
});

module.exports = router;
