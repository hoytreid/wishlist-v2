const express = require('express');
const router = express.Router();
const request = require('request-promise');
const apiController = require('../controllers/apiController');

let jwt;

router.use((req, res, next) => {
	if(jwt) {
		res.set({
			'Authorization': jwt
		});
	}
	next();
})

router.post('/register', (req, res) => {
	apiController.register(req, res)
		.then((response) => {
			res.send(response);
		})
		.catch((error) => {
			res.sendStatus(error.statusCode);
		});
});

router.post('/login', (req, res) => {
	apiController.login(req, res)
		.then((response) => {
			jwt = response.Authorization;
			res.send({token: jwt});
		})
		.catch((error) => {
			res.sendStatus(error.statusCode);
		});
});

router.get('/items', (req, res) => {
	apiController.getItems(res)
		.then((body) => {
			res.send(body._embedded);
		})
		.catch((error) => {
			res.sendStatus(error.statusCode);
		});
});

router.post('/items', (req, res) => {
	apiController.submitItem(req, res)
		.then((response) => {
			res.send(response.statusCode);
		})
		.catch((error) => {
			res.sendStatus(error.statusCode);
		});
});

router.put('/items/:id', (req, res) => {
	apiController.updateItem(req, res)
		.then((response) => {
			res.send(response.statusCode);
		})
		.catch((error) => {
			res.sendStatus(error.statusCode);
		});
});

router.delete('/items/:id', (req, res) => {
	apiController.deleteItem(req, res)
		.then((response) => {
			res.send(response.statusCode);
		})
		.catch((error) => {
			res.sendStatus(error.statusCode);
		});
});

module.exports = router;
