const express = require('express');
const bodyParser = require('body-parser');

const pageRoutes = require('./routes');
const apiRoutes = require('./routes/api');

const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({type:'application/json'}));

app.set('view engine', 'pug');

app.use(pageRoutes);
app.use('/api', apiRoutes);
app.use('/static', express.static('static'));

const port = 3000;

app.listen(port, () => {
	console.log(`server running on port ${port}`);
});
