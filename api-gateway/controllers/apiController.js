const request = require('request-promise');

module.exports = {
	getItems: function(res) {
		return new Promise((resolve, reject) => {
			request({
				uri: 'http://localhost:8080/api/items',
				json: true,
				headers: {
					'User-Agent': 'request-promise',
					'Authorization': res.get('Authorization')
				}
			}, (error, response, body) => {
				if(response.statusCode !== 200) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not get items");
			});
		});
	},

	getItemById: function(req, res) {
		return new Promise((resolve, reject) => {
			request({
				uri: 'http://localhost:8080/api/items/' + req.params.id,
				json: true,
				headers: {
					'User-Agent': 'request-promise',
					'Authorization': res.get('Authorization')
				}
			}, (error, response, body) => {
				if(response.statusCode !== 200) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not find item");
			});
		});
	},

	submitItem: function(req, res) {
		return new Promise((resolve, reject) => {
			request({
				method: 'POST',
				uri: 'http://localhost:8080/api/items',
				json: true,
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
					'Authorization': res.get('Authorization')
				},
				body: {
					'description': req.body.description,
					'price': req.body.price
				}
			}, (error, response, body) => {
				if(response.statusCode !== 201) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not add item");
			});
		});
	},

	updateItem: function(req, res) {
		return new Promise((resolve, reject) => {
			request({
				method: 'PUT',
				uri: 'http://localhost:8080/api/items/' + req.params.id,
				json: true,
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
					'Authorization': res.get('Authorization')
				},
				body: {
					'description': req.body.description,
					'price': req.body.price,
					'bought': req.body.bought
				}
			}, (error, response, body) => {
				if(response.statusCode !== 200) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not update item");
			});
		});
	},

	deleteItem: function(req, res) {
		return new Promise((resolve, reject) => {
			request({
				method: 'DELETE',
				uri: 'http://localhost:8080/api/items/' + req.params.id,
				headers: {
					'Authorization': res.get('Authorization')
				}
			}, (error, response, body) => {
				if(response.statusCode !== 204) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not delete item");
			});
		});
	},

	login: function(req, res) {
		return new Promise((resolve, reject) => {
			request({
				method: 'POST',
				uri: 'http://localhost:8080/api/accounts/login',
				json: true,
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: {
					'username': req.body.username,
					'password': req.body.password
				}
			}, (error, response, body) => {
				if(response.statusCode !== 200) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not log in");
			});
		});
	},

	register: function(req, res) {
		return new Promise((resolve, reject) => {
			if (req.body.username === '' || req.body.password === '') {
				return reject(new Error ("Please enter a username and password"));
			}

			request({
				method: 'POST',
				uri: 'http://localhost:8080/api/accounts/register',
				json: true,
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: {
					'username': req.body.username,
					'password': req.body.password
				}
			}, (error, response, body) => {
				if(response.statusCode !== 201) {
					reject(response);
				} else {
					resolve(body);
				}
			})
			.catch(() => {
				new Error("Could not register account");
			});
		});
	}
};
