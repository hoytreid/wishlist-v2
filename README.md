# Wishlist v2
A simple app for recording things you want, how much they are and what you've bought
## Using the app
Before using the app make sure that all links point to the right servers. The server running Express.js needs to be referenced in mobile-app/App.js - The current URL reference is on line 55.
```
const serverUrl = 'localhost:3000';
```
Be default the Express.js server runs on port 3000 and the SpringBoot server runs on port 8080.
